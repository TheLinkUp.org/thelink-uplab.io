---
title: About
---

## Mission

The Link-Up.org mission is to empower Veterans with a network of volunteers and organizations.

## Our Vision:

- Veterans find hunting and fishing partners more easily.
- Veterans make lasting memories and new friends.

## MEET THE TEAM:
- Bob Bowden, President, TheLink-Up.org
- Patricia Bowden, Financial Advisor
- Tyghe Vallard, Website Administrator


### Contact
Contact us with any questions, comments or suggestions at

Email: thelinkup.org@gmail.com
