---
title: Fishing for Life
countries:
- usa
statesprovinces:
- minnesota
categories:
- fishing
tags:
orgURL: https://fishingforlife.org
---
Fishing For Life has numerous programs and events through which we reach out to youth and families in our community. Each of our programs have been carefully designed to be a positive experience for you, whether the the event is large or small.
Examples of our events include: Holes for Heroes, Lake Events (our summer and winter lake fishing events), High C'sFishing Camps for youth, Fish Fair, our Military R&R ministry to wounded warriors and their families, and our Deep C's Men's Club. All of our events are designed to not only introduce us to one another, but find ways to deepen our relationships with you as well. 
