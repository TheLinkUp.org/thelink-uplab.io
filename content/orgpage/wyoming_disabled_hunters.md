---
title: Wyoming Disabled Hunters
countries:
- usa
statesprovinces:
- wyoming
categories:
- hunting
tags:
- antelope
- elk
- deer
orgURL: http://www.wyomingdisabledhunters.org
---
Wyoming Disabled Hunters is a 501c3 non-profit  organization founded by Wyoming residents who have a personal connection with physically challenged hunters.

Our mission is to make a difference in someone’s life providing an affordable hunt.

WDH provides room and board for all our hunters. The only out of pocket cost would be round trip expenditures to Wyoming and purchasing a hunting license.  WDH can help with these expenditures for hunters that qualify for financial assistance per our guidelines.  Ask for a Financial Assistance form when requesting information about the hunts we offer or download the form and send it with your application for a hunt.  Applicants who are 70% service related disabled veterans are eligible to get their license donated to them.  This option has also been opened up to civilians in a wheel chair.  This is an option for all 3 species we hunt.  Non veterans and non wheelchair bound hunters can apply to WDH and buy for their license after being drawn to hunt with WDH.


