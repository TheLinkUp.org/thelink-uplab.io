---
title: Walleyes for Wounded Heroes
countries:
- usa
statesprovinces:
- ohio
categories:
- fishing
tags:
- walleye
orgURL: http://walleyesforwoundedheroes.com/Default.aspx
---
Walleyes for Wounded Heroes, Inc., (W4WH) provides opportunities for our nation’s current or former uniformed members of the United States Armed Forces, sworn Law Enforcement Officers, sworn Fire Fighters and sworn or affirmed Emergency Medical Responders, all of whom have been injured from combat operations or line-of-duty service, to fish and reconnect with nature’s healing properties. Charter captains and private boat owners take our Wounded Heroes walleye fishing for a 4-day event annually during the last week of June at no charge. W4WH provides lodging, meals, entertainment, and transportation around Port Clinton, Ohio and nearby areas.

