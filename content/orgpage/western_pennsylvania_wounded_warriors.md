---
title: Western Pennsylvania Wounded Warriors
countries:
- usa
statesprovinces:
- pennsylvania
categories:
- hunting
- fishing
tags:
- deer
- turkey
orgURL: https://wpawoundedwarrior.org
---
In 2002, Bud West started the Western Pennsylvania Wounded Warriors. The original focus was helping paralyzed veterans through fishing until the group obtained the land lease in 2005. In 2007, the group became incorporated. Sadly, in 2010 Bud passed away. 

Today, we are a dedicated group of men and women who love the outdoors and believe that hunting and fishing should be accessible for everyone, regardless of disability. We serve both military and civilian disabled persons who enjoy hunting, fishing, and the outdoors. The only way they can do so is with the assistance of the Wounded Warrior volunteers who are physically able to do so. 
