---
title: The Sportsmen's Foundation for Military Families
countries:
- usa
statesprovinces:
- florida
- texas
categories:
- hunting
- fishing
tags:
- hogs
- boar
- coyote
orgURL: https://www.huntforvets.com
---
Our U.S. Armed Forces veterans and their families have sacrificed much for our freedom and our way of life. The Sportsmen’s Foundation for Military Families (SFMF) was established to honor that sacrifice. Eligible military veteran families are given the opportunity to participate in a professionally guided outdoor adventure. For the veteran family, an SFMF experience is often the trip of a lifetime and is extremely therapeutic for those who could use a helping hand in recovery.  It is our humble way of saying “thank you” for your service. We invite you to learn more about The Sportsmen’s Foundation for Military Families. “The number one best coping skill is the ability to maintain and put forth a positive attitude, especially in the face of adversity. SFMF hunts nurture that attitude and subsequently help combat veteran-hunters cope during the ups and downs of life."
