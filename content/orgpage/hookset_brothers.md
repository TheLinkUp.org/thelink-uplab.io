---
title: Hookset Brothers
countries:
- usa
statesprovinces:
- texas
categories:
- hunting
- fishing
tags:
orgURL: https://hooksetbrothers.org
---
Hookset Brothers Combat Recovery 501c3 provides free hunting and fishing trips to aide in their PTSD recovery.  We aim to share the therapeutic aspects the outdoors can provide while introducing a new sport and or way of life they can focus on instead of their struggles.  These trips are Once In A Life opportunities at Ranches throughout the State of Texas and or fishing some of the most prestigious Lakes in East Texas.  During such trips it’s an opportunity to get multiple Combat Veterans together pulling on the camaraderie we all once shared while serving.
