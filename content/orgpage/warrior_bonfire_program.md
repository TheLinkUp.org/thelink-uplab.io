---
title: Warrior Bonfire Program
countries:
- usa
statesprovinces:
- mississippi
categories:
- hunting
- fishing
tags:
orgURL: https://warriorbonfireprogram.org
---
The Warrior Bonfire Program is an organization created for service members who have been wounded in combat operations. The purpose of our programs are to encourage camaraderie between those that can fully understand the mindset of a combat wounded soldier and /or their spouse, foster healing through those relationships, inspire growth towards new passions and purpose post military service, or simply know they are not alone. Our programs center around hosting retreats that include therapeutic small group communications and activities such as hunting, fishing, skiing and more, with each event concluding with a bonfire. Our events are designed to set the examples of getting out with others to combat isolation and rejecting the use and need of self medication. Picture The Warrior Bonfire Program operates in Clinton, Mississippi, as a 501 ( c ) ( 3 ) tax exempt non profit charity. It is overseen by a board of directors of 7 members with experience in military operations, private sector businesses, non profit organizations, government processes, and entrepreneurial start ups. Currently, all funds raised go directly to the programs that support the combat wounded veterans we serve. The Warrior Bonfire Program has been in operation since January of 2013
