---
title: Chairbound Hunters of Wyoming
countries:
- usa
statesprovinces:
- wyoming
categories:
- hunting
tags:
- antelope
- deer
- elk
- turkey
orgURL: http://www.chairboundhunters.com
---
Chairbound Hunters is a small group of people committed to help give the chair bound hunter the opportunity to experience quality hunting in beautiful southeast Wyoming
Our reward is seeing the enjoyment our clients receive
Chairbound Hunters is a non-profit organization designed exclusively for persons who are wheelchair-bound, blind or terminally ill
These persons are taken out to hunt antelope, deer, elk and/or turkey by one of our board members or a qualified helper
We have hunters come from all over the United States
For now, our hunters have to buy their own hunting licenses, but the organization would eventually like to have scholarships for those who cannot afford to buy a hunting license.
