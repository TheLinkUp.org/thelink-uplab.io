---
title: Honored American Veterans Afield
countries:
- usa
statesprovinces:
- Massachusetts
categories:
- hunting
- shooting
tags:
orgURL: https://www.honoredveterans.org
---
HAVA was conceived and organized in 2007 by a Committee of shooting sports industry executives to help the healing and re-integration of disabled veterans and injured active military back into normal American life through participation in outdoor events. Seven companies committed funds and personnel to organize and sponsor initial HAVA sanctioned activities, and will serve on the Board of Directors, along with new Sustaining Sponsors for a period of three years to insure that the organization is launched in the proper manner.
The HAVA vision is the creation of a small organization of volunteers from the shooting sports industry to facilitate a series of hunting and shooting activities for groups of disabled veterans wherein personal attention of the sponsors and facility operators contributes to the veteran’s sense of joy and accomplishment, and a permanent awareness that marvelous things are possible despite disabling injuries. These veterans have given their full measure of commitment to the preservation of their country’s values, and deserve America’s contribution to their healing process to whatever degree necessary to accomplish physical, mental and cultural rehabilitation. HAVA, through the efforts of Sustaining Sponsors and other contributors, can become an inspiration to both the veteran and to a grateful nation whose best instincts are to support the veteran who has served its cause so well.
