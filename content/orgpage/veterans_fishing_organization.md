---
title: Veterans Fishing Organization
countries:
- usa
statesprovinces:
- Alabama
- Georgia
categories:
- hunting
- fishing
tags:
orgURL: https://www.vfohome.org
---
Veterans Fishing Organization, Inc. is a not for profit 501c3 organization.  We provide military Veterans, including but not limited to those with physical disabilities and Post-Traumatic Stress Disorder (PTSD), with an opportunity for recreation and renewal through guided fresh water fishing and wildlife observation on lakes in Georgia and Alabama.

