---
title: Flyin' Heroes
countries:
- usa
statesprovinces:
- michigan
categories:
- fishing
tags:
orgURL: https://flyinheroes.org
---
THE EXPERIENCE
Participants will experience a unique guided float trip down the Pere Marquette, Muskegon, and/or the White River located in West Michigan. The program also offers fishing trips on inland lakes throughout West Michigan. Each participant will be instructed on the basic techniques of fly and or light tackle fishing. Fishing activities are held from within a drift boat, on a riverbank or physically wading into the river depending on the comfort level of each participant.
