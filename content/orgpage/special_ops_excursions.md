---
title: Special Ops Excursions
countries:
- usa
statesprovinces:
- tennessee
categories:
- hunting
- fishing
tags:
orgURL: https://www.specialopsxcursions.org
---
Special Ops Xcursions was formed by a small group of friends in 2012 at their hunting club in Tennessee. They were looking for a way to share their love of the outdoors with others, and through mutual friends within the Army (Green Berets) the first group of soldiers arrived September 2013 for their annual club dove hunt. The overwhelming success of that first excursion quickly rippled into numerous events providing all types of outdoor activities. Hunting for ducks, geese, turkey, dove, & small game. Skeet & sporting clays shoots. Fishing for crappie, bluegill, catfish, bass, and on the fly. Zip lining in the Smokies. Horseback riding. Camping & hiking. Instructional classes starting for handguns up to long range tactical shooting.
We offer active duty Special Operations Forces (SOF) service members a chance to partner with professional guides & volunteers in the general public for endless opportunities in the outdoors. Through small group excursions, friendships are established that will last for years to come. A daily limit or full tag are just a bonus to the experience. A weekend away pre or post deployment is crucial to team and family well being. The outdoors can be a powerful healer.
