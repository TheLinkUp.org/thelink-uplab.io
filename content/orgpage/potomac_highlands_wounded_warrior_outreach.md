---
title: Potomac Highlands Wounded Warrior Outreach
countries:
- usa
statesprovinces:
- west virginia
categories:
- hunting
- fishing
tags:
orgURL: https://www.phwwo.com
---
TO PROVIDE OUTDOOR SPORTING OPPORTUNITIES TO OUR NATION'S WOUNDED VETERANS IN ORDER TO HELP THEIR HEALING  PROCESS, WELCOME THEM HOME AND REPAY A DEBT OF GRATITUDE FOR THE GREAT SACRIFICES THEY HAVE MADE.
