---
title: Wounded Warriors Weekend
countries:
- canada
- usa
statesprovinces:
- saskatchewan
categories:
- fishing
- golfing
- music
---

In August 2012, the town of Nipawin and the North East region of Saskatchewan opened their arms and welcomed 112 Wounded Warriors from Canada and the United States for a weekend of fishing, golfing, outdoor fun, musical performances, great food and lots of socializing.

The first Saskatchewan **"WOUNDED WARRIORS WEEKEND"** was a transforming weekend for everyone, those who were wounded and those who were there to help. The healing of damaged souls was visible as we saw how nature, music, compassion and support refreshed, nurtured and strengthened the delegates. Friendships were struck; families bonded; networks were created.

This was our time to give back. It was our intent, privilege and honour to show our troops and their families that they were not alone, that their sacrifices have not gone unnoticed that we stand by them, feel for their losses, and are indebted to them now and forever for their honorable service to our countries.
**Please join us once again as we present:**

WOUNDED WARRIORS WEEKEND 2013
AUGUST 2nd, 3rd, 4th and 5th

When we will welcome 120 Wounded Warriors from Canada, USA, the UK and Australia. Let us continue to show them that we are honoured by their sacrifices and they will always be remembered as heroes by those of us here at home.


