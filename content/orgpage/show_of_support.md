---
title: Show of Support
countries:
- usa
statesprovinces:
- texas
categories:
- hunting
- fishing
tags:
orgURL: http://www.showofsupport.org
---
Show of Support is a 501C3 non profit origanization honoring America's wounded veterans. Our purpose is to demonstrate public support for the men and women of our military by providing outdoor hunting adventures to those men and women who have been injured in service to our country. 

Started by Terry Johnson in 2004, the program also seeks to bring public awareness to the outdoor sports of hunting and fishing, respect for our resources and the care and preservation of those assets in addition to simply saying thanks.

In effort to show support, select U.S. Service Men and Women from different branches of our armed forces are honored for their services to this country at our Hunt for Heroes Banquet every year. The Banquet will be held prior to their departure to participate in an all expense paid whitetail deer hunt and the wives of our soldiers get a week of West Texas Hospitality. This is our way of saying "THANKS!" West Texas style.

Men and women from any branch of service who were injured and are discharged or will be discharged prior the scheduled hunt, are eligible for consideration to participate in the outdoor adventures that we offer.

These individuals have selflessly given their service to our nation to guarantee our safety and to protect our freedoms. There is not a more noble cause than what these individuals have taken upon themselves to do.  Their sense of honor, dignity, pride and sacrifice is what allows us to live in a nation that others clamor to our borders to be a part of.  They have volunteered to put their lives on the line for their beliefs, a free America.  Free from the threat of a segment of those who would want to see us living in fear.  This mighty nation will and is standing up to this terror threat thanks to these individuals.
