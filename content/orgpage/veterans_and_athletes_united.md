---
title: Veterans and Athletes United
countries:
- usa
statesprovinces:
- virginia
categories:
- retreat
- adaptive sports
- adaptive recreation
orgURL: https://www.vetsau.com
---
Veterans and Athletes United

VAU is an all-volunteer 501c3 non-profit run by veterans. We cover our less than 3% overhead costs through board donations to maximize every donated dollar towards our programs and those we serve. Founded in 2013 with a focus on providing much-needed support to disabled veterans and their family members through accessible retreats and adaptive sports/recreation. 
