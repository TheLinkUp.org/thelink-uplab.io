---
title: Deer, Turkey and Ducks Outdoors
countries:
- usa
statesprovinces:
- mississippi
categories:
- hunting
tags:
- deer
- turkey
- duck
orgURL: http://dtdoutdoors.webs.com/
---
D.T.D. Outdoors specific purpose is to raise funds in support of:
1. Educational Programs
2. Wildlife habitat enhancement and aquisition
3. Preservation of the hunting tradition and shooting sports for future generations.

In addition, D.T.D. Outdoors will work with local and state agencies to tackle the issues that are in dire need of care. D.T.D. Outdoors will also focus on the immediate needs of Wildlife Enhancement for Monroe County, MS and provide handicap children, United States Military Veterans, and the underprivileged more hunting opportunities that are current residents of Monroe County, MS.
