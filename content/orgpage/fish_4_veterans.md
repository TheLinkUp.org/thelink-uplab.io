---
title: Fish 4 Veterans
countries:
- usa
statesprovinces:
- california
categories:
- fishing
tags:
- fishing
orgURL: https://www.facebook.com/fish4veterans/
---
We provide free sport recreational therapy for disabled or financially deprived Veterans. Using bonding, networking outsourcing while teaching a skill to pass on to another. 
These adventures and voyages on the water/land give us the opportunity to donate the hunt or catch to low income churches/nonprofits
Mission is using the ocean to heal Veterans with sport recreational therapy methods. Specifically Helping PTSD and disabled Veterans with no cost to them. Simply Bonding together, healing while teaching the trade of fishing. Giving to those Veterans who normally couldn't afford deep sea fishing trips. Thus allowing us to catch quality food to give away to low income nonprofits/churches!
