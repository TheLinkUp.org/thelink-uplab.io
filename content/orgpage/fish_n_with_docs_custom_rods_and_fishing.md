---
title: Fish'n with Docs Custom Rods and Fishing
countries:
- usa
statesprovinces:
- texas
categories:
- fishing
tags:
- custom fishing rod
orgURL: https://fishinwithdocs.com/
---
## Mission
Fish’n with Docs Custom Rods & Fishing is a veteran based non-profit 501(c)3 organization dedicated to help combat veterans struggling with PTSD, anxiety disorder through fishing. It has been our long time dream to be able to reach out to combat veterans to help them with everyday struggles from combat PTSD, TBI, anxiety disorder, etc. With years of dealing with my own disorders and seeking help through therapy, we might be able to help a veteran with what I’ve learned and have them accept the very hard decision of asking for help by taking them out fishing and getting away from everyday struggles and enjoy nature. We hope that our efforts will help reduce the number of suicides in the servicemember and veteran community.
