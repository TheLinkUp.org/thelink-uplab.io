---
title: Freedom Fishing Foundation
countries:
- usa
statesprovinces:
- minnesota
- wisconsin
categories:
- fishing
tags:
orgURL: https://freedomfishingfoundation.com
---
We know it can feel lonely, but you’re not alone. We’re here to help you ride out the storm.
We are here to help vets and their families reconnect with nature and other people. It is SO much more than fishing.
Fishing is a tool. A favorite tool. One that we’ve found works well over the years.
If you’re a vet who’s struggling or know of one, or a family member of a vet, please apply to attend one of our fishing excursions.
Freedom Fishing Foundation provides fishing excursions anywhere from one to four days trips within Minnesota and Wisconsin. All military branches are included and may apply. Minor military children must be accompanied by an adult guardian/parent. Veterans are allowed to bring one, possibly two guests depending on the type of fishing with prior approval. We encourage you to apply for yourself or on behalf of a vet that would benefit.
