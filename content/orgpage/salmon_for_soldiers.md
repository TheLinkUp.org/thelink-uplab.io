---
title: Salmon for Soldiers
countries:
- usa
statesprovinces:
- washington
categories:
- fishing
tags:
orgURL: https://www.salmonforsoldiers.com
---
“The Mission of Salmon for Soldiers is to offer a sense of normalcy and relaxation to our nation's Veterans through fishing.  Salmon for Soldiers' fishing opportunities are designed to help reduce stress while creating new relationships with others who love fishing.  Our events are designed to accommodate Veterans with paralysis, PTSD, TBI and other debilitating challenges."
