---
title: Serve Outdoors Matagorda Bay Chapter
countries:
- usa
statesprovinces:
- texas
categories:
- hunting
- fishing
tags:
orgURL: http://www.serveoutdoorsmbc.org/home.html
---
Serve Outdoors is a 501c3 nonprofit organization that assists disabled individuals, veterans, wounded veterans, youth, special needs folks and the elderly to experience the great outdoors on hunting and fishing adventures.

Our Mission is simple, find the resources to take folks huntin' & fishin'. But not just regular folks, extraordinary folks! Folkswho don't believe in layin' down when faced with a challenge.Folks who have served our nation. From veterans to disabled folks, seniors to children, if there is a barrier that you need assistance with to put you back out into the great outdoors, we are here for you!
