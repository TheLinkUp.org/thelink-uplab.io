---
title: Team Vet
countries:
- usa
statesprovinces:
- alabama
categories:
- hunting
- fishing
tags:
- veterans
- first responders
- children with disabilities
orgURL: https://www.facebook.com/teamvet
---
Using fishing and hunting to reach veterans, first responders and children that suffer from mental or physical disabilities whether temporary or permanent. We do this for no cost to them
