---
title: Operation Patriots FOB
countries:
- usa
statesprovinces:
- south carolina
categories:
- hunting
- fishing
tags:
orgURL: https://opfob.org
---
Our mission as an organization is dedicated to creating and fostering positive experiences for Combat Veterans by connecting through outdoor and recreational activities. We create an alliance through peer engagement in a comfortable and relaxed environment.
Each and every Combat Veteran has a purpose beyond their call of duty. We are committed to helping them find it.
