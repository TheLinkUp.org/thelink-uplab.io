---
title: Wounded Warriors United
countries:
- usa
statesprovinces:
- kansas
- minnesota
- wisconsin
categories:
- hunting
- fishing
- camping
tags:
- hog
- deer
- pheasant
- ice fishing
orgURL: http://www.woundedwarriorsunited.com
---
Wounded Warriors United is a not for profit organization exclusively founded for our combat wounded and combat veterans. Wounded Warriors United is based in Manhattan, KS.

Wounded Warriors United provides all-inclusive events at no cost to our wounded warriors. Wounded Warriors United provides events to our heroes to include, hunting, fishing, camping, and numerous sporting and social events. These events are therapeutic and will assist in the healing of our heroes hearts and minds.
Wounded Warriors United wants to increase public awareness on the effects the outdoors has on our wounded warriors mental and physical disabilities. Wounded Warriors United relies exclusively on private and corporate donations.

Over 95% of donations directly benefit our wounded warriors
