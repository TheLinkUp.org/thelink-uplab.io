---
title: NEA (Northeast Arkansas) Wounded Warriors Group
countries:
- usa
statesprovinces:
- arkansas
categories:
- hunting
- fishing
tags:
orgURL: https://www.facebook.com/nea.woundedwarriors/?fref=ts
---
NEA Wounded Warriors was created to help veterans who have been injured to enjoy the sport of hunting, as well as visit and share stories with others.
 
*Mission*
The NEA Wounded Warrior Group is a non-profit, all volunteer organization with a mission to enhance the quality of life forUnited States of America's military service members. We conduct activities and sporting events for NEA Wounded Warriors and other disabled veterans within the group's capablity. A Wounded Warrior is a veteran that is receiving disability pay from the DOD or is enrolled in the VA Healthcare system. NEA consists of Clay, Craighead, Greene, Lawrence, and Randolph counties of Arkansas. Disabled active service members are welcome and encouraged to attend our sporting events!
