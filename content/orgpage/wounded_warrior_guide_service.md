---
title: Wounded Warrior Guide Service
countries:
- usa
statesprovinces:
- minnesota
- texas
- missouri
- louisiana
categories:
- hunting
- fishing
tags:
- hogs
- turkey
- paddlefish
- duck
- goose
orgURL: https://wwgsmn.org
---
Established in 2009, We provide outdoor recreational activities for disabled veterans of the United States Armed Forces, these activities and related expenses are provided at no cost. We offer year round adventures that include fishing both summer and winter, large and small game hunting, long range shooting, backpacking/camping, and more.
