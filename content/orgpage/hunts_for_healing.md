---
title: Hunts for Healing
countries:
- usa
statesprovinces:
- pennsylvania
categories:
- hunting
- fishing
tags:
orgURL: https://huntsforhealing.org
---

Hunts for Healing is a small non-profit, volunteer organization dedicated to recreating the small squad experience for wounded veterans through mentored outdoor challenges. If you are a post 9-11 combat vet who was injured in the line of duty or who is struggling to adjust to life stateside, our goal is to help you find some healing in the great outdoors.
We host a variety of multi-day events year-round during which all of your needs are accommodated including home-cooked meals, supplies and sleeping quarters. Our events are limited to small groups and every participant is paired with a mentor. While our mentors and volunteers come from all walks of life and backgrounds, many are vets themselves.
Our hope is that through this experience we can aid in the physical, emotional and spiritual healing of wounded vets and help ease the transition back into daily life.
