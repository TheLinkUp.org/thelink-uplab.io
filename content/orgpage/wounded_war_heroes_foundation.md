---
title: Wounded War Heroes Foundation
countries:
- usa
statesprovinces:
- louisiana
categories:
- hunting
- fishing
tags:
orgURL: https://woundedwarheroes.org
---
Wounded War Heroes' (WWH) pledges that these gallant men and women who have sacrificed so much for our country and sustained life-altering wounds or injuries as a result of serving our country in the fight for freedom are distinguished for their courageous service and sacrifice. Our passion for the outdoors and desire to give generously by demonstrating everyone's appreciation supporting the Wounded Ware Heroes' Vision, to enrich a person's beneficial outreach to support and fund therapeutic outdoor experiences for these warriors. 
