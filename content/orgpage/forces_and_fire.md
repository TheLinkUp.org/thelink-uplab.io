---
title: Forces and Fire
countries:
- usa
statesprovinces:
- iowa
categories:
- hunting
- fishing
tags:
- deer
- turkey
orgURL: http://www.forcesandfire.org/home
---
Forces and Fire’s Inc. mission is to provide a safe outdoor recreational experience for any disable/combat veteran, firefighter or law enforcement officer and their families, where they can utilize their strengths and their independence. We will show them that they have not been forgotten and will give them an opportunity to succeed, and realize their contribution to their community and family. Forces and Fire Inc. was founded on the belief that it our mission to help any veteran, firefighter, or law enforcement officer that was injured in the line of duty to be able to enjoy a outdoor recreational experience with other people that are going thought the same experience they’re going though. We will encourage that families be part of this experience. Forces and Fire Inc. would like to give a veteran, firefighter, or law enforcement office the ability to enjoy a outdoor active with their family or by themselves that would not be possible because of the injury they has received, do to their line of duty injury. 
