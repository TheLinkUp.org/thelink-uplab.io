---
title: Wounded Warrior Anglers
countries:
- usa
statesprovinces:
- colorado
- washington
- wisconsin
categories:
- hunting
- fishing
tags:
orgURL: http://www.woundedwarrioranglers.org
---
Wounded Warrior Anglers of America Incorporated is aNationally Chartered 501(C) (3) Public Charity, which wasfounded in 2012 by David Souders (a Wounded Warrior) and Judy Souders (his Wife & Caregiver). The Co-Founders have dedicated themselves to helping others going through like situations andhave now turned David’s passion for fishinginto a means ofhelping other Wounded Warriors realize they are still capable of enjoying life,along withan active lifestyle after life changing injuries.
Ourprimary mission is to help rehabilitate the mind, body, & soul of all service members who have been injured, wounded or disabled in the line of duty no matter what their era of service. Our secondary missions are to actively support the wounded warrior'scaregiver and thier immediate family.
Our primary mission is carried out by actively mentoring wounded warrior's with fishing therapy and comradery. These fishing trips actively promote a friendly and peaceful environment that helps our warriors heal by being around other veterans who understand their mental & physical issues.
Our secondary mission is carried out by actively mentoring caregivers through a relaxing day away while their warrior is fishing. These missions are accomplished by stress free day at the local Spa and actively promote a healing and peaceful environment and helps caregivers themselves deal with their daily burdens by being around other caregivers who understand their daily struggles.
We hope to bring inspiration and confidence to our Wounded Warrior through teaching fishing techniques and the development of new friendships that can be sustained for a lifetime.
