---
title: Wheelin' Sportsmen (NWTF Sponsored)
countries:
- usa
statesprovinces:
- alabama
- alaska
- arizona
- arkansas
- california
- colorado
- connecticut
- delaware
- florida
- georgia
- hawaii
- idaho
- illinois
- indiana
- iowa
- kansas
- kentucky
- louisiana
- maine
- maryland
- massachusetts
- michigan
- minnesota
- mississippi
- missouri
- montana
- nebraska
- nevada
- new hampshire
- new jersey
- new mexico
- new york
- north carolina
- north dakota
- ohio
- oklahoma
- oregon
- pennsylvania
- rhode island
- south carolina
- south dakota
- tennessee
- texas
- utah
- vermont
- virginia
- washington
- west virginia
- wisconsin
- wyoming
categories:
- hunting
- fishing
tags:
- deer
- turkey
orgURL: https://www.nwtf.org/programs/wheelin-sportsmen
---
The Wheelin’ Sportsmen program began in October 2000 as the NWTF recognized the need to help people with mobility impairments enjoy the outdoors by participating in hunting and shooting sports.

Wheelin’ Sportsmen events provide participants an opportunity they may not be able to have on their own due to the lack of hunting land access, lack of the knowledge of how to return to the field after an injury or lack of necessary assistance.

Oftentimes, the knowledge and experience gained at one of our events allows the participants to continue hunting on their own throughout the year.
