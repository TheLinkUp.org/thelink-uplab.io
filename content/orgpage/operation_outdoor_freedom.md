---
title: Operation Outdoor Freedom
countries:
- usa
statesprovinces:
- florida
categories:
- hunting
- fishing
tags:
- turkey
- deer
- bass
- salt water
- waterfowl
- ducks
- geese
orgURL: https://www.fdacs.gov/Forest-Wildfire/Our-Forests/State-Forests/State-Forest-Recreation/Operation-Outdoor-Freedom?original_host=www.operationoutdoorfreedom.com/
---
Supported by Florida Commissioner of Agriculture Nicole "Nikki" Fried and led by the Florida Forest Service, Operation Outdoor Freedom provides recreational and rehabilitative opportunities to wounded veterans on state forests, agricultural lands and private lands throughout Florida at no cost. Since its inception in 2009, Operation Outdoor Freedom has hosted more than 500 events and served approximately 4,100 wounded veterans.
