---
title: Let's Go Fishing
countries:
- usa
statesprovinces:
- minnesota
- wisconsin
categories:
- fishing
tags:
orgURL: https://lgfws.com
---
Let’s Go Fishing has developed its mission over time.  Early on we felt that we best served our communities by focusing on the camaraderie of bringing older adults on fishing trips, sharing the fun of the catch.  As our organization developed, we came to realize that by reaching out to older adults, veterans and the disabled, we not only were able to bring fun but simply by bringing people to nature we were contributing to their health and well being.

There has been much written about the beneficial effects that nature has on our mental and physical health and so we have seen it proven true time and time again.  Therefore we made an important change to our mission statement, encompassing the factors of health and well-being in what we do:

