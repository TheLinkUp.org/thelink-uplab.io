---
title: Halo for Freedom Warriors Foundation
countries:
- usa
statesprovinces:
- texas
categories:
- hunting
tags:
- hog
orgURL: https://www.haloforfreedom.org
---
## Our Mission
*HALO for Freedom Warrior Foundation* was started to show that physically challenged people can still work and excel in today's society and military. The foundation emphasizes that "amputee" and "useless" are not synonymous. The organization provides support for wounded warriors as they face the many challenges they encounter during the rehabilitation, reintegration, and healing process.

## Humble Beginnings
Based on a simple idea, the first Wounded Warrior Hog Hunt was held in the fall of 2007 with 10 wounded warriors and 70 volunteers. Dana hit the rural streets to find a place to have the hunt, and several people stepped up and provided the venue. Dana Bowman was able to secure $7500.00 in donations to accomplish the task. Dana and Ken Tucker started cutting metal and welding up several stands that would be used by the warriors to hunt hogs and coyotes. Several dog teams from West Texas joined us, along with a chuck wagon chef. The pre hunt get together was held in Dana's living room and the following morning the teams set out before sunrise. In the end 4 hogs were harvested and the Warriors, staff and volunteers parted ways as friends. Back in 2007 we were just a humble group of caring people that wanted to give back to the Veterans that had been injured fighting for us. We always felt we could do more......

