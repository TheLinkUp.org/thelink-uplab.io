---
title: Veteran Angler Charters
countries:
- usa
statesprovinces:
- connecticut
- massachusetts
categories:
- fishing
tags:
- fishing
orgURL: http://www.veterananglercharters.org
---
Veteran Angler Charters is an all-volunteer, federally recognized 501(c)(3) non-profit organization. We offer free, small group charter fishing trips to injured and recovering veterans from all conflicts and from all branches of the armed forces.  Our mission is to provide our veterans with recreational rehabilitation and therapeutic support.
