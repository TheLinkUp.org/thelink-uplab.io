---
title: Freedom Waters Foundation
countries:
- usa
statesprovinces:
- florida
categories:
- boating
- fishing
tags:
- boating
- fishing
orgURL: https://freedomwatersfoundation.org
---
Freedom Waters Foundation provides boating and fishing opportunities for veterans and their family members, offering the opportunity to relax, leave their concerns on the dock and enjoy camaraderie with other veterans in a safe, enriching environment.
