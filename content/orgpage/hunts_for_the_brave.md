---
title: Hunts for the Brave
countries:
- usa
statesprovinces:
- utah
categories:
- hunting
- fishing
tags:
orgURL: https://www.huntsforthebrave.org
---
To honor and acknowledge the service and sacrifices of the “Brave.”  The “Brave” includes members of our Armed Forces who defend our freedom; Public Servants who have performed a heroic act; or youth fighting to overcome serious ailments. This will be accomplished by providing memorable outdoor recreational opportunities for the “Brave” as a way of rewarding their service or heroism and providing an opportunity to heal their mind and spirit in a beautiful outdoor setting.

