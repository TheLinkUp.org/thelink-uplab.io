---
title: Wild Warrior Adventure
countries:
- usa
statesprovinces:
- wyoming
categories:
- fishing
tags:
- flyfishing
orgURL: https://www.wildwarrioradventure.org
---
Wild Warrior Adventure (WWA) exists to encourage those who have served / continue to serve in the U.S. Armed Forces, have risked their lives to defend our freedom and have sustained wounds, including the invisible wounds of combat operational stress injuries, and the stress of the rigors of military service.

We serve our Warriors and their families through providing spiritual outdoor adventures in exceptional environments, away from the distractions of daily life, to find freedom from bondage, to build emotional and spiritual resiliency, and to lead healthy, God-centered lives.

Wild Warrior Adventure partners with various military organizations and commands such as Military Chaplaincy, Veteran’s Administration Staff, The Master’s Program, other Veteran Service Organizations, and our Alumni to identify Veterans and Service Members who would benefit from the WWA Adventure.

Through the generous support of individuals, organizations, and corporations, we are able to fund the transport of attendees to our outdoor adventure locations where they will be housed, fed (physically, emotionally and spiritually), and treated like VIPs. As Alumni of WWA, they will always be a part of our family as we continue to offer aftercare programs and opportunities for ongoing fellowship.

It has been proven that the adventure of fly fishing and outdoor activities, great food, hospitality, and fellowship around the fire can create an environment where significant conversations take place. These intentional experiences and conversations lead to healing, freedom, and a renewed hope. This creates the environment and bond that allow our Alumni to return home, encouraged to serve their families, communities, and fellow Veterans and Service Members.
