---
title: Hunting with a Hero
countries:
- usa
statesprovinces:
- ohio
- maine
- texas
- oklahoma
- south carolina
- washington
- alabama
categories:
- hunting
- fishing
tags:
- bear
- deer
- boar
- dove
- elk
- goose
- turkey
orgURL: http://huntingwithahero.org
---
Hunting With A Hero knows that help and healing for service members with PTSD and their families doesn’t just stop in clinical therapy. Our goal is to open non-hospital care centers for service members and their families, free of charge, outside every US military installation, where the whole family can learn to better understand how PTSD affects them, and learn ways to cope together.
