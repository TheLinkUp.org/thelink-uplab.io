---
title: Take a Soldier Fishing
countries:
- usa
statesprovinces:
- oregon
categories:
- hunting
- fishing
tags:
orgURL: https://www.takeasoldierfishing.org
---
​​​​We assist communities at large to show their support for veterans and activie duty military  by sponsoring our Take A Soldier/Veteran Fishing events.  These events honor our active duty military and veterans for their service to our nation and illustrate that our communities appreciate their sacrifice.  Volunteers that support our fishing programs help make profound differences in active service members or veterans lives by providing a safe and positive outdoor experience.
 
We strive to serve all veterans and active duty military  of all abilities and life situations.  Our intention is to leave every veteran and active service member who participates in our events with a memorable experience that gives them pride in wearing their uniform, provides hope for their future, and a general positive outlook.
