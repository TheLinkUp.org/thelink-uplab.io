---
title: Wisconsin Hero Outdoors
countries:
- usa
statesprovinces:
- wisconsin
categories:
- hunting
- fishing
tags:
- SCUBA
- fishing
- hunting
orgURL: https://wiherooutdoors.org
---
Here at, Wisconsin Hero Outdoors, we serve our Veteran and First Responder community in 3 primary ways:

We work hand in hand with the Veterans Affairs (VA) Medical System, including the In-Patient Residential Mental Health Program, Housing and Urban Development-Veterans Administration Supportive Housing (HUD-VASH) Departments, and Recreational Therapy Departments to provide outdoor excursions for our Veterans under the care of the VA. Currently, we work with the Milwaukee, Madison, Tomah, and North Chicago VA.
Coordination with Veteran Service Organizations and Non-Government Organizations to provide an outdoor activity element to their organizations. Examples include our collaboration with Veterans Outreach of Wisconsin, AMVETS, American Legion, United Special Sportsman’s Alliance, Combat Veterans Motorcycle Association, Vets on the Fly, and a variety of local police and fire departments.
Lastly and possibly most important, we conduct group outings with individual and small groups of Veterans and/or First Responders from hunting and fishing excursions to golf, SCUBA diving, and equestrian programs.
