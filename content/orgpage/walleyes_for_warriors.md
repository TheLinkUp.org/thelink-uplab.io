---
title: Walleyes for Warriors
countries:
- usa
statesprovinces:
- michigan
categories:
- fishing
tags:
orgURL: http://www.walleyesforwarriors.org
---
Walleyes for Warriors was founded by Nels Larsen, an avid Saginaw Bay walleye fisherman and Vietnam veteran (1968-1970). The idea for Walleyes for Warriors came from a similar veteran's fishing event Nels attended in Manistee, Michigan called Tight Lines for Troops.  It was such an emotional day, a "Welcome Home" experience many vets  didn't get to experience when they returned home from war. Nels wanted to be able to have other vets experience the same thing.  That's where the idea for Walleyes for Warriors began. Nels has since transitioned the event to Bay City Elks Lodge #88. We are honored to present this event for our area veterans. Saginaw Bay is an exceptional walleye fishery and we want to be able to share this experience with our veterans.

