---
title: Outdoor Experience for All
countries:
- usa
statesprovinces:
- arizona
categories:
- hunting
- fishing
tags:
- children
- deer
- antelope
- elk
orgURL: http://www.outdoorexperienceforall.org
---
The concept for The Outdoor Experience 4 All was conceived in late 2007. Although there are charitable organizations whose focus is offering hunts to terminally ill children, they tend to focus on hunts that require extensive travel and time commitments. We recognized that there were many families of children with life-threatening illnesses who could not take an extended hunt. Many of these families have other children, and long for an experience that could involve the entire household or even extended family. Due to the prohibitive cost and time involved with interstate travel, we realized that our focused needed to be on Arizona families.

After many months of planning we were able to put our first hunt together in September of 2008. Not knowing how it would go we decided for our first hunt to call on a youth we already knew who would qualify for a big game tag transfer. We had hunted with Thomas Widenhofer during a 2007 elk hunt and we had a strong relationship with his family so he was a natural choice. Thomas agreed to be our first hunter and with many months of planning he arrived with the hopes of harvesting an Arizona antelope. Within the first hour of the hunt Thomas had harvested a nice antelope that measured 84 4/8. The sense of accomplishment felt by everyone involved lit a fire and The Outdoor Experience 4 All was off and running!

From September of 2008 till the end of the year we were able to do one antelope hunt, four deer hunts, and five elk hunts, and took two children with autism fishing. After the fishing trip we realized that we should not limit this organization just to youths who have been diagnosed with a life threatening illness, but also offer an outdoor experience to those who have disabilities, non-life threatening illnesses, and the children of our fallen heroes. So in the fall of 2008 we became officially incorporated as a non-profit 501(c)3.

The mission of the Outdoor Experience 4 All (OE4A) is to change lives one adventure at a time. Everyone who participates in an OE4A adventure, including volunteers, sponsors, parents, and siblings leaves camp with a new outlook on life!
