---
title: Rocky Mountain Heroes Foundation
countries:
- usa
statesprovinces:
- colorado
- wyoming
categories:
- hunting
- fishing
tags:
- pronghorn antelope
- deer
- elk
orgURL: https://rmheroes.org
---
 Rocky Mountain Heroes Foundation’s mission is to help disabled US military veterans and their children (families) based in the Rocky Mountain states to reintegrate into civilian life by building a sense of community, camaraderie, and self-confidence. The program engages in volunteer and outreach events to build connections between our members, their families, and our communities. We use hunting and fishing adventures to build family connections and provide therapy to our members. We provide our members and their families the opportunity to participate in field-based adventures at a level appropriate to their skill level and physical ability, building their self-confidence, relieving stress and enhancing family bonds. 
