---
title: Take a Vet Fishing
countries:
- usa
statesprovinces:
- wisconsin
- illinois
- minnesota
- michigan
categories:
- fishing
tags:
orgURL: http://takeavetfishing.org
---
Our Mission Statement
 
To honor, comfort and assist our PTSD (Post Traumatic Stress Disorder) and DAV (Disabled American Veteran) soldiers.
Our Vision
Take a Vet Fishing is a 501 (C) (3) not for profit organization that was created in 2011 to provide emotional support and rehabilitation to those who serve our great nation every day. The mission is simple, to show these men and women how much they mean to us and support them and their families with (emotional) PTSD (Post Traumatic Stress Disorder) and (Physical) DAV (Disabled American Veterans) outings and trips.
 
Our Purpose
 
Our slogan is “A day of giving back” and is the core of our mission. When our brave service men a women return from overseas, as a nation, we have been failing to show these heroes our appreciation. A shockingly high number of service members are returning with injuries and even the soldiers without physical wounds are having a hard time adjusting back into the “real world”. These “Hero’s” have given so much of themselves for our freedom. Reaching out and offering a “Thank You” with a hand shake, is the very least we can do. At Take a Vet Fishing, we take it a step further. Not only do we show these hero’s our gratitude and shake their hand, we hand them a fishing rod and ease their minds back into civilization.

