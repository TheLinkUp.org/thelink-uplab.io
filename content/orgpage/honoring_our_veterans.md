---
title: Honoring Our Veterans
countries:
- usa
statesprovinces:
- wyoming
categories:
- photography
- fishing
tags:
orgURL: https://www.honorvets.org
---
fly-fishing, white-water rafting, paddling sports/therapy, horseback riding, wildlife tours, Yellowstone and Grand Teton Park tours, scenic rafting on the Snake River, Barbecue dinners and campfire chats with staff, rehabilitation professionals, and local residents. The veterans have found that these activities and the associated socialinteraction have helped them re-integrate into society more easily.

