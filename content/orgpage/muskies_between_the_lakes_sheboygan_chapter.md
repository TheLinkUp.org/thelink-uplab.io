---
title: Muskies Between the Lakes Sheboygan Chapter
countries:
- usa
statesprovinces:
- wisconsin
categories:
- fishing
tags:
orgURL: http://www.betweenthelakes20.com/veterans-outing
---
Annual veterans outing.
We are proud to thank our veterans for their time and sacrifice on behalf of the United States in this small way.
There is not cost to participate, all veterans are welcome.
Event is held on Saturday the 3rd weekend of September.
Event is held on Random Lake.
We meet at the pavilion at 8 am. 
After a brief breakfast and few words, we hit the water,
Each Vet will have a guide.
We fish til noon, then head in for some food and socializing.
We end the day with a group photo, flag presentations, and a few door prizes.
