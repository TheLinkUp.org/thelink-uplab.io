---
title: Montana Warriors on the Water
countries:
- usa
statesprovinces:
- montana
categories:
- fishing
tags:
orgURL: http://www.montanawarriorsonthewater.org/?p=1
---
Montana Warriors on the Water (MWOTW) is a non-profit 501 (c) (3) organization, comprised of local sportsmen and women, and veterans of the United States Military, partnered with local businesses, communities, and organizations, providing outdoor recreational therapy and opportunities to veterans of the United States Military at no cost to them.

In addition to honoring our veterans by giving back, it is our goal to provide an environment for our veterans to relax and be able to meet other veterans from across the country who may be combat wounded or suffering from issues associated with the psychological effects of war. By connecting with each other and with individuals, private companies, non-profit organizations and Government agencies, a community has been put in place to honor and support the vast growing number of veterans.

Our goal is to ensure our veterans continue to have physical access to the country they served, in a mentally and emotionally stable environment, and are honored for their commitment and service to this nation.

We at MWOTW are mission oriented in our objective to honor and support our nation’s veterans. In war, there are no unwounded soldiers…..
