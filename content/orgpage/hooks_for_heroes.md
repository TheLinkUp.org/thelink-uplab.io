---
title: Hooks For Heros
countries:
- usa
statesprovinces:
- alaska
categories:
- fishing
tags:
- soldiers
- veterans
- first responders
- gold star families
orgURL: https://www.hooks4heroes.com
---
MAKING A DIFFERENCE ONE CAST AT A TIME.

We take Soldiers, Veterans, First Responders, and Gold Star Families out fishing. These events honor our active duty military and veterans for their service to our nation and illustrate that our communities appreciate their sacrifice and struggles. Our goal is to build resilience in service members and reduce suicide by building connection to one another. Volunteers that support our fishing programs help make profound differences in active service members or veterans lives by providing a safe and positive outdoor experience and often times lasting friendships.

Our intention is to leave every person who participates in our events with a memorable experience that gives them pride, provides hope for their future, and a general positive outlook
