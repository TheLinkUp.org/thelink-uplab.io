---
title: House in the Woods
countries:
- usa
statesprovinces:
- maine
categories:
- hunting
- fishing
- kayaking
- canoeing
- hiking
tags:
orgURL: https://www.houseinthewoods.org
---
Our Mission is to create a therapeutic, recreational, and educational retreat for our nation's U.S. armed forces and their families, using outdoor wilderness activities in Maine and natural habitats to help participants share common challenges related to their service and sacrifice in protecting our nation's freedom and democratic ideals.
