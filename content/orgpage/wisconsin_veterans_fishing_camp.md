---
title: Wisconsin Veterans Fishing Camp
countries:
- usa
statesprovinces:
- wisconsin
categories:
- fishing
tags:
orgURL: https://wivetfishcamp.wordpress.com
---
TThe “Wisconsin Veterans Fishing Camp” was established to give the opportunity to Veterans from around the state of Wisconsin to attend a fishing camp dedicated to them. This is our way of showing our HEROES how THANKFUL we are for their service to our country, as well as for our FREEDOM.

