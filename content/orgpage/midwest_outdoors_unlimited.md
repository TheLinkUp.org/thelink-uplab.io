---
title: Midwest Outdoors Unlimited
countries:
- usa
statesprovinces:
- Illinois
- Minnesota
categories:
- hunting
tags:
- deer
- elk
orgURL: https://www.midwestoutdoorsunlimited.com
---
Our mission is to provide outdoor recreational activities for Disabled American Veterans, disabled individuals and disabled youth in Minnesota. Because of their disabilities, the individuals and groups that we help don’t have enough funds, volunteers or help to do these things. Often times these handicapped people, who are family, friends, and neighbors, do not have the opportunity to do many of the activities that others enjoy. Our goal is to help them to enjoy these actives that people who don’t have disabilities enjoy and to improve their quality of life, to help them feel they are not forgotten. Be it lack of funds or volunteers or the handicap accessible places to enjoy the outdoor recreation that others do.
