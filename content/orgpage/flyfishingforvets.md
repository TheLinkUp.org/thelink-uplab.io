---
title: Project Healing Waters - Atlanta
countries:
- usa
statesprovinces:
- Georgia
categories:
- fishing
tags:
- flyfishing
orgURL: https://phw-atlanta.org/
---
Project Healing Waters Fly Fishing - Atlanta is a non-profit foundation dedicated to the wounded warriors and their families. Our goals are to use fly fishing as a means of healing and recovery for our veterans.
