---
title: Freedom Hunters
countries:
- usa
statesprovinces:
- colorado
categories:
- hunting
tags:
- deer
- bear
- mountain lion
orgURL: http://www.freedomhunters.org
---
About
Saluting the noble work of our courageous men and women of the Armed Forces is our mission. Freedom Hunters reflects the outdoor community's appreciation to the troops by taking select active duty and combat veterans on outdoor adventures.

Mission
Our mission is to honor and support all members of the United States Military and their families.

Company Overview
Freedom Hunters is a 501(c)3 military outreach program dedicated to honoring those who protect our Freedoms. Our mission is to salute the noble work of our courageous men and women of our Armed Forces. Freedom Hunters reflects the outdoor community’s appreciation to our troops by taking: select active duty and combat veterans, families of fallen heroes, children of the deployed, as well as those wounded or injured, on outdoor adventures. Empowered by help from conservation groups, outfitters, corporations, government agencies, and landowners, Freedom Hunters honors individuals from all branches of the military. It is with immense pride and enthusiasm that Freedom Hunters carries on the American tradition of hunting, fishing, and marksmanship. EIN: 45-5388947
