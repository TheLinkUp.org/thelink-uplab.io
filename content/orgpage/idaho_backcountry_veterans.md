---
title: Idaho Backcountry Veterans
countries:
- usa
statesprovinces:
- idaho
categories:
- hunting
- fishing
tags:
- elk
- deer
- salmon
orgURL: https://backcountryveterans.org
---
Idaho Backcountry Veterans (IBV) is a nonprofit created for our veterans to enjoy quality hunting, fishing and other sporting activities

### Our Approach
We get vets together with each other and away from the daily grind. Like minded individuals in a comfortable environment where they can drop the shields and open up about their experiences with those that have been through the same experiences.

### Our Story
Started in 2017 Idaho Back Country Veterans is a Not-For-Profit corporationregistered in Idaho. We fundraise and have a series of amazing sponsors and partners that help us create naturalescapes for those we help.

