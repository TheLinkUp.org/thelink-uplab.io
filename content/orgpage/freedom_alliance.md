---
title: Freedom Alliance
countries:
- usa
statesprovinces:
- virginia
categories:
- hunting
- fishing
tags:
orgURL: https://freedomalliance.org
---
Freedom Alliance is an educational and charitable organization which sponsors numerous programmatic activities aimed at supporting and honoring our service members and their families. Freedom Alliance is recognized by the Internal Revenue Service (IRS) as a public charity organized under Section 501(c)(3) of the IRS Code.
Our mission is to advance the American heritage of freedom by honoring and encouraging military service, defending the sovereignty of the United States and promoting a strong national defense.
## Support Our Troops
Every day Freedom Alliance pays tribute to the bravery and sacrifice of the men and women who wear our nation’s uniform and fight for our freedom. We do this through our Support Our Troops program, a year-round outreach which honors members of our Armed Forces – those who have been wounded in combat and are recuperating at military hospitals throughout the United States – as well as those currently serving on the frontlines. We also provide support and comfort to grieving military families, whose loss is immeasurable.
## Troops Appreciation Events
Freedom Alliance has no shortage of creative supporters who host events to benefit our Support Our Troops program. These events include golf tournaments, car shows, bake sales, outdoor sporting contests, and Poppy’s Wish Heroes Vacations to name a few.

