---
title: Hook Line and Heroes
countries:
- usa
statesprovinces:
- north carolina
categories:
- fishing
tags:
orgURL: https://hooklineandheroes.org
---
Serving those who have served us by providing an unforgettable outdoor experience, through fishing with disabled and post-traumatic stress (PTS) military veterans and heroes in our lives.

Sharing God’s love in the beauty of His great creation, while learning to fish with a professional angler, creating an unforgettable experience on the water.

Leaving filled with many blessings God provides through the outdoors.
