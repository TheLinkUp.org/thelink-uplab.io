---
title: Hunters Helping Hunters
countries:
- usa
statesprovinces:
- Connecticut
categories:
- hunting
- fishing
tags:
- hunting
- assistance
orgURL: https://www.huntershelpinghuntersusa.org/mission-assistance/mission-assistance.html
---

# Mission Statement
Hunters Helping Hunters USA is an organization developed to assist families that have had an interruption in the family structure or support. Founded by fellow hunters and outdoorsmen from across the country, Hunters Helping Hunters USA will assist families associated with hunting with medical or funeral costs due to a family member’s death or medical situation. 

The assistance will help offset the family’s financial burden caused by the cost of such an interruption. Through contributions, donations and other financial aids, HHH-USA's goal is to provide the funds to help alleviate some of the financial issues during these critical times, thus allowing the family to focus more on rebuilding the family structure and support system.
