---
title: Veteran Excursions to Sea Inc
countries:
- usa
statesprovinces:
- florida
categories:
- fishing
tags:
- red drum
- snook
- trout
- jacks
- tarpon
- shark
orgURL: https://veteransexcursions.org
---
- We offer free fishing trips for veterans and their family (up to 3 guests).
- Veteran must provide a DD-214 for proof of military service.
- These excursions are inshore/near shore trips. targeting Red Drum, Snook, Trout, and Tarpon. (May/June Only) .
- Shark and Grouper trips available upon request.
- Honorably discharged veterans are welcome, However, priority is given to our Combat Veterans.
- Purple Heart Recipients will be provided accommodations if needed.
- We fish in Charlotte Harbor, The World Famous Boca Grande Pass and surrounding areas.
- The only thing we ask in return is that you, Reel in some memories.

Tyler Crane
Port Charlotte, FL
