![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

Source repository that powers http://TheLink-Up.org and http://thelink-up.gitlab.io

## Adding new organizations

1. Head over to https://gitlab.com/thelink-up/thelink-up.gitlab.io/-/tree/master/content/page/orgpages
1. Click the `+`and add a new file
1. Name the file so that it doesn't have any spaces or special charaters(remove special characters and replace spaces with `-`)
1. Paste in the content from the template below into that page
1. Change any of the fields, remove any fields, add content below the bottom `---` line

```
---
title: Organization Name
countries:
- canada
- usa
statesprovinces:
- montana
- texas
categories:
- hunting
- fishing
tags:
- really
- anything
- you
- want
orgURL: http://www.theorg.com/something/else
---
any info on the org in markdown content format
```

## Adding new menu links

Edit the `config.yaml` file and add links under the menu section

## Adding new pages

Add new pages under the `content/pages` directory. You can use the `about.md` file as an example.

You can then add it as a menu option similar to how the `About` page is added in the `config.yaml` file
