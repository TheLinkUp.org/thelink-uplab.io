#!/usr/bin/env python

## Used to get all the orgpage htmls
## further processing will come next in the next script

import requests
import pathlib
import re
import string
import argparse
import urllib

def get_page(link):
    r = requests.get(link)
    return r.text

def get_orgpage():
    return get_page('https://www.thelink-up.org/page/organizations-1')
    return r.text

def get_usergenerated(html):
    divs = 0
    capture = False
    lines = []
    for line in html.splitlines():
        if divs == 0:
            capture = False

        if '<div class="xg_user_generated">' in line:
            capture = True

        if '<div' in line and capture:
            divs += line.count('<div')
        if '</div>' in line and capture:
            divs -= line.count('</div>')

        if capture:
            lines.append(line)
    return '\n'.join(lines)

def parse_ahrefline(ahrefline):
    '''
    >>> line = '<p><a href="https://example.com/thing#asd" target="_blank" rel="noopener">the text</a></p> '
    >>> parse_ahrefline(line)
    ('https://example.com/thing#asd', 'the text')
    '''
    r = re.search('<a href="(.*)" target="_blank" rel="noopener">(.*)</a>', ahrefline)
    if r:
        return r.groups()
    return ('', '')

def get_link(link):
    html = requests.get(link)
    return html

def normalize_name(name):
    '''
    >>> x = 'this\\'s a g#*!&d'
    >>> normalize_name(x)
    'this_s_a_g____d.md'
    '''
    for c in string.punctuation:
        name = name.replace(c, '_')
    name = name.replace(' ', '_')
    return name + '.md'

def clean_html(html):
    tags = [
        '<p>',
        '</p>',
        '<span>',
        '</span>',
        '&nbsp;',
        '<span class="font-size-3">',
        '<div id="page-content" class="description"><div class="xg_user_generated">',
    ]
    for tag in tags:
        html = html.replace(tag, '')
    return html

def frontmatter(title):
    return f'''---
title: {title}
countries:
- usa
statesprovinces:
- montana
categories:
- hunting
- fishing
tags:
- really
- anything
- you
- want
orgURL: http://www.theorg.com/something/else
---
'''

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
                '--orgpage',
                default=""
            )
    parser.add_argument(
                '--force-refetch',
                default=False,
                action='store_true',
                help='Refetch page even if it is already downloaded'
            )
    return parser.parse_args()

def get_page_title_from_html(page_html):
    r = re.search('<title>(.*)</title>', page_html)
    if r:
        title = r.group(1).replace(' - TheLink-Up.org', '').strip()
        return title
    raise Exception('could not find title')

def get_page_title_from_url(url):
    u = urllib.parse.urlparse(url)
    path = u.path.replace('/page/', '')
    title = normalize_name(path)
    return title

def get_single_orgpage(url, force_refetch):
    pagename = get_page_title_from_url(url)
    page = pathlib.Path(f'orgpages/{pagename}')
    if not force_refetch and page.exists():
        print(f'{page} already exists so skipping')
        return
    if not url.startswith('https://www.thelink-up.org/page'):
        print(f'{url} does not start with https://www.thelink-up.org/page')
        return
    print(f'Fetching {url}')
    page_html = get_page(url)
    title = get_page_title_from_html(page_html)
    content = get_usergenerated(page_html)
    content = frontmatter(title) + (content)
    page.parent.mkdir(parents=True, exist_ok=True)
    print(f'writing {page}')
    page.write_text(content)

if __name__ == '__main__':
    args = parse_args()
    if args.orgpage:
        get_single_orgpage(args.orgpage, args.force_refetch)
    else:
        html = get_orgpage()
        lines = get_usergenerated(html)
        for line in lines.splitlines():
            p = parse_ahrefline(line)
            if p[0]:
                get_single_orgpage(p[0], args.force_refetch)
